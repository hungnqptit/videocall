import React from 'react';
import {Dimensions} from 'react-native';
import ActionButton from 'react-native-action-button';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

let dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const FluteButton = props => {
  const {onButton, data} = props;
  return (
    <ActionButton
      buttonColor="rgba(0,0,0,0.4)"
      size={(dimensions.width / 360) * 44}
      useNativeFeedback={false}
      offsetX={dimensions.width / 12}
      offsetY={dimensions.height / 22}>
      <ActionButton.Item
        buttonColor="rgba(0,0,0,1)"
        useNativeFeedback={false}
        onPress={() => {
          onButton(0);
        }}>
        <MaterialIcons
          name={data[0]}
          size={(dimensions.width / 360) * 25}
          color="white"
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="rgba(0,0,0,1)"
        useNativeFeedback={false}
        onPress={() => {
          onButton(1);
        }}>
        <MaterialIcons
          name={data[1]}
          size={(dimensions.width / 360) * 25}
          color="white"
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="rgba(0,0,0,1)"
        onPress={() => {
          onButton(2);
        }}>
        <MaterialIcons
          name={data[2]}
          size={(dimensions.width / 360) * 25}
          color="white"
        />
      </ActionButton.Item>
    </ActionButton>
  );
};

export default FluteButton;
