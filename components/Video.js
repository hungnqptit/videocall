/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */

import React, { Component } from 'react';
import { View, StyleSheet, NativeModules, Text, Dimensions, TouchableOpacity, StatusBar, ToastAndroid} from 'react-native';
import { RtcEngine, AgoraView  } from 'react-native-agora';
import CameraRoll from '@react-native-community/cameraroll';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import FluteButton from './FluteButton';
import { captureScreen } from 'react-native-view-shot';

console.disableYellowBox = true;
const { Agora } = NativeModules;

const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Adaptative,
} = Agora;

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peerIds: [],
      uid: Math.floor(Math.random() * 100),
      appid: this.props.AppID,
      channelName: this.props.ChannelName,
      vidMute: false,
      audMute: false,
      joinSucceed: false,
      lastMin:0,
      lastSec:0,
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
      startDisable: false,
    };
    this.t = 0;
    const config = {
      appid: this.state.appid,
      channelProfile: 0,
      videoEncoderConfig: {
        width: 720,
        height: 1280,
        bitrate: 2000,
        frameRate: FPS30,
        orientationMode: Adaptative,
      },
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
    };
    RtcEngine.init(config);
  }

  captureScreenFunction=()=>{
    captureScreen({
      format: 'jpg',
      quality: 0.8,
    }).then(
      uri => {
        CameraRoll.saveToCameraRoll(uri,'photo');
        ToastAndroid.show('Lưu', ToastAndroid.SHORT, ToastAndroid.BOTTOM, ToastAndroid.CENTER);
      },
      error => {}
    );
  }

  increaseTime=()=>{
    let timer = setInterval(() => {
      var num = (Number(this.state.seconds_Counter) + 1).toString(),
        count = this.state.minutes_Counter;
      if (Number(this.state.seconds_Counter) === 59) {
        count = (Number(this.state.minutes_Counter) + 1).toString();
        num = '00';
      }
      this.setState({
        minutes_Counter: count.length === 1 ? '0' + count : count,
        seconds_Counter: num.length === 1 ? '0' + num : num,
      });
    }, 1000);
    this.setState({ timer });
    this.setState({startDisable : true});
  }

  componentWillUnmount(){
    clearInterval(this.state.timer);
  }

  componentDidMount() {
    RtcEngine.on('userJoined', (data) => {
      const { peerIds } = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        this.setState({
          peerIds: [...peerIds, data.uid],
        });
        this.setState({
          timer: null,
          minutes_Counter: '00',
          seconds_Counter: '00',
        });
        this.increaseTime();
      }
    });
    RtcEngine.on('userOffline', (data) => {
      this.setState({
        peerIds: this.state.peerIds.filter(uid => uid !== data.uid),
      });
      clearInterval(this.state.timer);
      this.setState({startDisable : false});
    });
    RtcEngine.on('joinChannelSuccess', (data) => {
      RtcEngine.startPreview();
      this.setState({
        joinSucceed: true,
      });
    });
    RtcEngine.joinChannel(this.state.channelName, this.state.uid);
    RtcEngine.enableAudio();
  }

  onFluteButton = (id) => {
    if (id === 1) {
      this.switchCamera();
    } else
    if (id === 2) {this.captureScreenFunction();}
    else {this.toggleAudio();}
  };

  switchCamera=()=>{
    RtcEngine.switchCamera();
  }

  toggleAudio=()=> {
    let mute = this.state.audMute;
    RtcEngine.muteLocalAudioStream(!mute);
    this.setState({ audMute: !mute });
  }

  toggleVideo() {
    let mute = this.state.vidMute;
    this.setState({ vidMute: !mute });
    RtcEngine.muteLocalVideoStream(!this.state.vidMute);
  }

  endCall() {
    RtcEngine.destroy();
    Actions.home();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        <View style={{ width: dimensions.width, backgroundColor:this.state.peerIds.length > 0 ? 'transparent' : 'black' }}>
          <Text style={styles.time}>{`${this.state.minutes_Counter}:${this.state.seconds_Counter}`}</Text>
        </View>
        {
          this.state.peerIds.length > 0
          ? <AgoraView style={{ flex: 1 }} remoteUid={this.state.peerIds[0]} mode={1} />
          : <Text style={styles.txt}>No users connected</Text>
        }
        {
          !this.state.vidMute && <View style={[styles.localVideoStyle, {backgroundColor: 'transparent', zIndex: 1}]}  />
        }
        {
          !this.state.vidMute
          ? <AgoraView style={styles.localVideoStyle2} zOrderMediaOverlay={true} showLocalVideo={true} mode={1}/>
          : <View style={[styles.localVideoStyle, {backgroundColor: 'black', zIndex: 1}]}  />
        }
        <View style={styles.buttonBar}>
          <TouchableOpacity
              onPress={() => this.toggleVideo()}
              style={[styles.menu,{ backgroundColor: 'rgba(0,0,0,0.4)'}] }
            >
              <Icon name={this.state.vidMute ? 'videocam-off' : 'videocam'} size={dimensions.width / 360 * 30} color="white" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.endCall()}
              style={[styles.menu, {backgroundColor:'red'}]}
            >
              <Icon name="call-end" size={dimensions.width / 360 * 30} color="white" />
            </TouchableOpacity>
            <View style={styles.menu} />
        </View>
        <FluteButton
            data={[ this.state.audMute ? 'mic-off' : 'mic','switch-camera', 'camera' ]}
            onButton={this.onFluteButton}
        />
      </View>
    );
  }
}

let dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const styles = StyleSheet.create({
  buttonBar: {
    width: dimensions.width / 100 * 85,
    position: 'absolute',
    bottom: dimensions.height / 22,
    left: dimensions.width / 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0.0)',
  },
  localVideoStyle: {
    width: dimensions.width / 100 * 34,
    height: dimensions.height / 100 * 26,
    // borderWidth: dimensions.width / 100 * 2.01,
    // borderColor: 'white',
    // borderRadius: 20,
    position: 'absolute',
    top: dimensions.height / 100 * 2,
    right: dimensions.width / 100 * 3,
  },
  localVideoStyle2: {
    // borderWidth: dimensions.width / 100 * 2.01,
    // borderColor: 'white',
    position: 'absolute',
    top: dimensions.height / 100 * 2,
    right: dimensions.width / 100 * 3,
    zIndex: 0,
    width: dimensions.width / 100 * 30,
    height: dimensions.height / 100 * 24.8,
    marginTop: dimensions.height / 100 * 0.65,
    marginRight: dimensions.width / 100 * 2,
  },
  txt: {
    textAlign: 'center',
    textAlignVertical: 'center',
    height: dimensions.height,
    width: dimensions.width,
    backgroundColor: 'black',
    color: 'white',
    fontSize: dimensions.width / 360 * 17,
    fontWeight: 'bold',
  },
  menu:{
    width: dimensions.width / 360 * 45,
    height:dimensions.width / 360 * 45,
    borderRadius: dimensions.width / 360 * 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  time:{
    alignSelf:'center',
    color:'white',
    backgroundColor: 'rgba(0,0,0,0.3)',
    fontSize: 20,
  },
});

export default Video;
