/* eslint-disable prettier/prettier */
import { Router, Scene, ActionConst } from 'react-native-router-flux';
import React from 'react';
import Video from './Video';
import Home from './Home';

const RouterComponent = () => (
	<Router>
		<Scene>
			<Scene key="home" component={Home} title="Viện Nhi" initial type={ActionConst.RESET} />
			<Scene key="video" component={Video} title="Cuộc gọi video" type={ActionConst.RESET} hideNavBar={true} />
		</Scene>
	</Router>
);

export default RouterComponent;
